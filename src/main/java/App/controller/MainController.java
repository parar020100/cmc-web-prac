package App.controller;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.*;


import DAO.*;
import entities.*;
import entities.Thread;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Cookie;
import javax.validation.constraints.Null;

@Controller
public class MainController {

    /*
     * Helper functions
     */

    private User authUser(String login, String password)
    {
        UserDAO user_manage = new UserDAO();
        User user = user_manage.findByLogin(login);
        if (user != null && user.getPassword().equals(password))
            return user;
        return null;
    }

    private void setAuthParameters(Model model,
                                   @CookieValue(value = "auth_login", defaultValue = "") String auth_login,
                                   @CookieValue(value = "auth_password", defaultValue = "") String auth_password) {
        User auth = authUser(auth_login, auth_password);
        if (auth != null) {
            model.addAttribute("auth_nickname", auth.getNickname());
            model.addAttribute("auth_id", auth.getUser_id());
            if (auth.isIs_moderator())
                model.addAttribute("auth_moderator", true);
            if (auth.isIs_banned())
                model.addAttribute("auth_banned", true);
        }


    }


    /*
     * Main pages
     */


    @GetMapping({"/", "/main_page"})
    public String viewMainPage(Model model,
                               @CookieValue(value = "auth_login", defaultValue = "") String auth_login,
                               @CookieValue(value = "auth_password", defaultValue = "") String auth_password)
    {
        setAuthParameters(model, auth_login, auth_password);
        UserDAO user_manage = new UserDAO();
        List<User> users = user_manage.getAllSorted();

        SectionDAO section_manage = new SectionDAO();
        List<Section> sections = section_manage.getAllSorted();

        model.addAttribute("users", users);
        model.addAttribute("sections", sections);

        model.addAttribute("pagename", "главная страница");

        return "main_page";
    }

    @GetMapping({ "/section" })
    public String viewSection(@RequestParam int section_id, Model model,
                              @CookieValue(value = "auth_login", defaultValue = "") String auth_login,
                              @CookieValue(value = "auth_password", defaultValue = "") String auth_password)
    {
        setAuthParameters(model, auth_login, auth_password);

        UserDAO user_manage = new UserDAO();
        SectionDAO section_manage = new SectionDAO();
        ThreadDAO thread_manage = new ThreadDAO();

        Section current_section = section_manage.findById(section_id);
        List<User> users = user_manage.getFromSectionSorted(current_section);


        List<Thread> threads = thread_manage.getBySectionSorted(current_section);

        model.addAttribute("users", users);
        model.addAttribute("threads", threads);
        model.addAttribute("description", current_section.getDescription());
        model.addAttribute("section_id", current_section.getSection_id());

        model.addAttribute("pagename", "Раздел " + current_section.getName());

        return "section";
    }

    @GetMapping({ "/profile" })
    public String viewProfile(@RequestParam int user_id, Model model,
                              @CookieValue(value = "auth_login", defaultValue = "") String auth_login,
                              @CookieValue(value = "auth_password", defaultValue = "") String auth_password)
    {
        setAuthParameters(model, auth_login, auth_password);

        UserDAO user_manage = new UserDAO();
        User current_user = user_manage.findById(user_id);

        MessageDAO message_manage = new MessageDAO();
        List<Message> messages = message_manage.getAllFromUser(current_user);

        if (current_user != null)
            model.addAttribute("user", current_user);
        if (messages != null)
            model.addAttribute("messages", messages);

        model.addAttribute("pagename", "профиль пользователя " + current_user.getNickname());

        return "profile";
    }

    @GetMapping({ "/thread" })
    public String viewThread(@RequestParam int thread_id, Model model,
                             @CookieValue(value = "auth_login", defaultValue = "") String auth_login,
                             @CookieValue(value = "auth_password", defaultValue = "") String auth_password)
    {
        setAuthParameters(model, auth_login, auth_password);

        MessageDAO message_manage = new MessageDAO();
        ThreadDAO thread_manage = new ThreadDAO();
        Thread thread = thread_manage.findById(thread_id);
        List<Message> messages = message_manage.getAllFromThread(thread);



        model.addAttribute("messages", messages);
        model.addAttribute("pagename", "обсуждение \"" + thread.getName() + "\"");
        model.addAttribute("thread", thread);

        return "thread";
    }





    /*
     * Additional pages
     */


    @GetMapping({ "/authorization" })
    public String viewLogIn(Model model,
                            @CookieValue(value = "auth_login", defaultValue = "") String auth_login,
                            @CookieValue(value = "auth_password", defaultValue = "") String auth_password)
    {
        User auth = authUser(auth_login, auth_password);
        if (auth != null)
            return "redirect:main_page";

        return "authorization";
    }

    @PostMapping({ "/authorization" })
    public String LogIn(WebRequest webRequest, HttpServletResponse response) {

        String login = webRequest.getParameter("login");
        String password = webRequest.getParameter("password");

        User user = authUser(login, password);

        if(user == null)
            return "redirect:authorization_fail";

        if (user.isIs_banned())
            return "redirect:authorization_banned";

        Cookie login_cookie = new Cookie("auth_login", login);
        Cookie password_cookie = new Cookie("auth_password", password);
        response.addCookie(login_cookie);
        response.addCookie(password_cookie);

        return "redirect:main_page";
    }

    @GetMapping({ "/authorization_fail" })
    public String viewFailedLogIn(Model model,
                            @CookieValue(value = "auth_login", defaultValue = "") String auth_login,
                            @CookieValue(value = "auth_password", defaultValue = "") String auth_password)
    {
        User auth = authUser(auth_login, auth_password);
        if (auth != null)
            return "redirect:main_page";

        model.addAttribute("auth_failed", true);

        return "authorization";
    }

    @GetMapping({ "/authorization_banned" })
    public String viewBannedLogIn(Model model,
                                  @CookieValue(value = "auth_login", defaultValue = "") String auth_login,
                                  @CookieValue(value = "auth_password", defaultValue = "") String auth_password)
    {
        model.addAttribute("auth_banned", true);

        return "authorization";
    }



    @GetMapping({ "/logout" })
    public String showLogOut(Model model)
    {
        model.addAttribute("logout", true);
        return "logout";
    }

    @PostMapping({ "/logout" })
    public String logOut(WebRequest webRequest, HttpServletResponse response) {

        Cookie login_cookie = new Cookie("auth_login", null);
        Cookie password_cookie = new Cookie("auth_password", null);
        Cookie nickname_cookie = new Cookie("auth_nickname", null);

        login_cookie.setMaxAge(0);
        password_cookie.setMaxAge(0);
        nickname_cookie.setMaxAge(0);

        response.addCookie(login_cookie);
        response.addCookie(password_cookie);
        response.addCookie(nickname_cookie);

        return "redirect:main_page";
    }



    @GetMapping({ "/new_user" })
    public String newUser(Model model,
                            @CookieValue(value = "auth_login", defaultValue = "") String auth_login,
                            @CookieValue(value = "auth_password", defaultValue = "") String auth_password)
    {
        User auth = authUser(auth_login, auth_password);
        if (auth != null)
            return "redirect:main_page";

        return "new_user";
    }

    @PostMapping({ "/new_user" })
    public String createUser(WebRequest webRequest, HttpServletResponse response) {

        UserDAO user_manage = new UserDAO();

        String login = webRequest.getParameter("new_user_login");
        String password = webRequest.getParameter("new_user_password");
        String password_confirm = webRequest.getParameter("new_user_repeat_password");
        String nickname = webRequest.getParameter("new_user_nickname");


        if (! password.equals(password_confirm) )
            return "redirect:new_user_fail";

        if (login == null || password == null || nickname == null)
            return "redirect:new_user_fail";

        if (user_manage.findByLogin(login) != null)
            return "redirect:new_user_fail";

        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        User new_guy = new User(login, password, nickname, date);
        user_manage.addUser(new_guy);

        return "redirect:authorization";
    }

    @GetMapping({ "/new_user_fail" })
    public String viewFailedCreateUser(Model model,
                                  @CookieValue(value = "auth_login", defaultValue = "") String auth_login,
                                  @CookieValue(value = "auth_password", defaultValue = "") String auth_password)
    {
        User auth = authUser(auth_login, auth_password);
        if (auth != null)
            return "redirect:main_page";

        model.addAttribute("creation_failed", true);
        return "new_user";
    }

    @GetMapping({ "/new_section" })
    public String viewCreateNewSection(Model model,
                                       @CookieValue(value = "auth_login", defaultValue = "") String auth_login,
                                       @CookieValue(value = "auth_password", defaultValue = "") String auth_password)
    {
        User auth = authUser(auth_login, auth_password);
        if (auth == null || auth.isIs_moderator() == false)
            return "redirect:main_page";
        return "new_section";
    }

    @PostMapping({ "/new_section" })
    public String createNewSection(WebRequest webRequest, HttpServletResponse response,
                                   @CookieValue(value = "auth_login", defaultValue = "") String auth_login,
                                   @CookieValue(value = "auth_password", defaultValue = "") String auth_password) {

        User auth = authUser(auth_login, auth_password);
        if (auth == null || auth.isIs_moderator() == false)
            return "redirect:main_page";

        SectionDAO section_manage = new SectionDAO();

        String name = webRequest.getParameter("new_section_name");
        String description = webRequest.getParameter("new_section_desc");

        if (section_manage.findByName(name) != null)
            return "redirect:main_page";

        java.sql.Timestamp time = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());

        Section new_section = new Section(auth, name, description, time);
        section_manage.addSection(new_section);

        return "redirect:section?section_id=" + new_section.getSection_id();
    }

    @GetMapping({ "/new_thread" })
    public String viewCreateNewThread(@RequestParam int section_id,
                                      Model model,
                                      @CookieValue(value = "auth_login", defaultValue = "") String auth_login,
                                      @CookieValue(value = "auth_password", defaultValue = "") String auth_password)
    {
        SectionDAO section_manage = new SectionDAO();
        List<Section> sections = section_manage.getAll();

        model.addAttribute("sections", sections);
        model.addAttribute("section_id", section_id);

        return "new_thread";
    }

    @PostMapping({ "/new_thread" })
    public String createNewThread(WebRequest webRequest, HttpServletResponse response,
                                  @CookieValue(value = "auth_login", defaultValue = "") String auth_login,
                                  @CookieValue(value = "auth_password", defaultValue = "") String auth_password)
    {

        SectionDAO section_manage = new SectionDAO();
        ThreadDAO thread_manage = new ThreadDAO();
        MessageDAO message_manage = new MessageDAO();
        String section_id_string = webRequest.getParameter("new_thread_section");

        if (section_id_string == null)
            return "redirect:main_page";

        Integer section_id = Integer.parseInt(section_id_string);
        Section current_section = section_manage.findById(section_id);

        if(current_section == null)
            return "redirect:main_page";

        User auth = authUser(auth_login, auth_password);
        if (auth == null)
            return "redirect:section?section_id=" + current_section.getSection_id();

        String name = webRequest.getParameter("new_thread_name");
        String firstmsg_text = webRequest.getParameter("new_thread_firstmsg");

        if (thread_manage.findByName(current_section, name) != null)
            return "redirect:section?section_id=" + current_section.getSection_id();

        java.sql.Timestamp time = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());

        Thread new_thread = new Thread(auth, current_section, name);
        thread_manage.addThread(new_thread);

        Message first_msg = new Message(new_thread, auth, new_thread, name, firstmsg_text, time);
        message_manage.addMessage(first_msg);

        new_thread.setInitial_message(first_msg);
        thread_manage.updateThread(new_thread);

        return "redirect:thread?thread_id=" + new_thread.getThread_id();
    }

    @GetMapping({ "/delete_section" })
    public String deleteSection(@RequestParam int section_id,
                                Model model,
                                @CookieValue(value = "auth_login", defaultValue = "") String auth_login,
                                @CookieValue(value = "auth_password", defaultValue = "") String auth_password)
    {
        User auth = authUser(auth_login, auth_password);
        if (auth == null || auth.isIs_moderator() == false)
            return "redirect:main_page";

        SectionDAO section_manage = new SectionDAO();

        section_manage.deleteSection(section_manage.findById(section_id));

        return "redirect:main_page";
    }

    @GetMapping({ "/delete_thread" })
    public String deleteThread(@RequestParam int thread_id,
                                Model model,
                                @CookieValue(value = "auth_login", defaultValue = "") String auth_login,
                                @CookieValue(value = "auth_password", defaultValue = "") String auth_password)
    {
        ThreadDAO thread_manage = new ThreadDAO();
        Thread thread = thread_manage.findById(thread_id);
        Section section = thread.getParent_section();

        User auth = authUser(auth_login, auth_password);
        if (auth == null || auth.isIs_moderator() == false)
            return "redirect:section?section_id=" + section.getSection_id();


        MessageDAO message_manage = new MessageDAO();


        thread.setInitial_message(null);
        thread_manage.updateThread(thread);

        message_manage.deleteAllFromThread(thread);
        thread_manage.deleteThread(thread);

        return "redirect:section?section_id=" + section.getSection_id();
    }

    @GetMapping({ "/delete_message" })
    public String deleteMessage(@RequestParam int message_id,
                               Model model,
                               @CookieValue(value = "auth_login", defaultValue = "") String auth_login,
                               @CookieValue(value = "auth_password", defaultValue = "") String auth_password)
    {

        MessageDAO message_manage = new MessageDAO();

        Message msg = message_manage.findById(message_id);

        Thread thread = msg.getParent_thread();

        User auth = authUser(auth_login, auth_password);
        if (auth == null || auth.isIs_moderator() == false)
            return "redirect:thread?thread_id=" + thread.getThread_id();

        message_manage.deleteMessage(msg);
        return "redirect:thread?thread_id=" + thread.getThread_id();
    }

    @GetMapping({ "/ban_user" })
    public String banUser(@RequestParam int user_id,
                                Model model,
                                @CookieValue(value = "auth_login", defaultValue = "") String auth_login,
                                @CookieValue(value = "auth_password", defaultValue = "") String auth_password)
    {
        User auth = authUser(auth_login, auth_password);
        if (auth == null || auth.isIs_moderator() == false)
            return "redirect:profile?user_id=" + user_id;

        UserDAO user_manage = new UserDAO();
        User user = user_manage.findById(user_id);
        user.setIs_banned(true);
        user_manage.updateUser(user);

        return "redirect:profile?user_id=" + user_id;
    }
    @GetMapping({ "/pardon_user" })
    public String pardonUser(@RequestParam int user_id,
                          Model model,
                          @CookieValue(value = "auth_login", defaultValue = "") String auth_login,
                          @CookieValue(value = "auth_password", defaultValue = "") String auth_password)
    {
        User auth = authUser(auth_login, auth_password);
        if (auth == null || auth.isIs_moderator() == false)
            return "redirect:profile?user_id=" + user_id;

        UserDAO user_manage = new UserDAO();
        User user = user_manage.findById(user_id);
        user.setIs_banned(false);
        user_manage.updateUser(user);

        return "redirect:profile?user_id=" + user_id;
    }

    @PostMapping({ "/send_message" })
    public String sendMessage(@RequestParam int thread_id,
                              WebRequest webRequest,
                              HttpServletResponse response,
                              @CookieValue(value = "auth_login", defaultValue = "") String auth_login,
                              @CookieValue(value = "auth_password", defaultValue = "") String auth_password) {

        ThreadDAO thread_manage = new ThreadDAO();
        MessageDAO message_manage = new MessageDAO();

        Thread thread = thread_manage.findById(thread_id);

        User auth = authUser(auth_login, auth_password);
        if (auth == null)
            return "redirect:thread?thread_id=" + thread.getThread_id();



        String title = webRequest.getParameter("new_message_title");
        String text = webRequest.getParameter("new_message_text");
        java.sql.Timestamp time = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());

        Message new_msg = new Message(thread, auth, null, title, text, time);

        message_manage.addMessage(new_msg);

        return "redirect:thread?thread_id=" + thread.getThread_id();
    }

}
