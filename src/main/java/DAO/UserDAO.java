package DAO;

import entities.User;
import entities.Section;
import entities.Thread;
import entities.Message;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import utils.HibernateSessionFactory;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


public class UserDAO
{
    public User findById(int id)
    {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        User user = session.get(User.class, id);
        session.close();
        return user;
    }

    public User findByLogin(String login)
    {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();

        Query<User> query = session.createQuery("FROM User WHERE login = :login", User.class);
        query.setParameter("login", login);


        if (query.getResultList().size() == 0) {
            session.close();
            return null;
        }


        User user = (User) query.getSingleResult();
        session.close();

        return user;
    }

    public void addUser(User user)
    {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction T = session.beginTransaction();
        session.save(user);
        T.commit();
        session.close();
    }

    public void updateUser(User user)
    {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction T = session.beginTransaction();
        session.update(user);
        T.commit();
        session.close();
    }

    public void deleteUser(User user)
    {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction T = session.beginTransaction();
        session.delete(user);
        T.commit();
        session.close();
    }

    public List<User> getAll() {
        List<User> users;
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        session.beginTransaction();

        Query<User> query = session.createQuery("FROM User", User.class);
        users = (List<User>) query.list();

        session.getTransaction().commit();
        session.close();
        return users;
    }

    public List<User> getAllSorted() {
        List<User> users;
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        session.beginTransaction();

        Query<User> query = session.createQuery("FROM User ORDER BY getRecentUserActivity(user_id, 730) DESC", User.class);
        users = (List<User>) query.list();

        session.getTransaction().commit();
        session.close();
        return users;
    }

    public List<User> getFromSectionSorted(Section section) {
        List<User> users;
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        session.beginTransaction();

        Query<User> query = session.createQuery("FROM User ORDER BY getUserSectionActivity(user_id, :section_id) DESC", User.class);
        query.setParameter("section_id", section.getSection_id());
        users = (List<User>) query.list();

        session.getTransaction().commit();
        session.close();
        return users;
    }

}