package DAO;

import entities.User;
import entities.Section;
import entities.Thread;
import entities.Message;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import utils.HibernateSessionFactory;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


public class ThreadDAO
{
    public Thread findById(int id)
    {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Thread thread = session.get(Thread.class, id);
        session.close();
        return thread;
    }

    public Thread findByName(Section section, String name)
    {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();

        Query<Thread> query = session.createQuery("FROM Thread WHERE name = :name AND parent_section = :section", Thread.class);
        query.setParameter("name", name);
        query.setParameter("section", section);

        if (query.getResultList().size() == 0) {
            session.close();
            return null;
        }
        Thread thread = (Thread) query.getSingleResult();
        session.close();

        return thread;
    }

    public void addThread(Thread thread)
    {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction T = session.beginTransaction();
        session.save(thread);
        T.commit();
        session.close();
    }

    public void updateThread(Thread thread)
    {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction T = session.beginTransaction();
        session.update(thread);
        T.commit();
        session.close();
    }

    public void deleteThread(Thread thread)
    {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction T = session.beginTransaction();

        session.delete(thread);

        T.commit();
        session.close();
    }

    public List<Thread> getAll() {
        List<Thread> threads;
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        session.beginTransaction();

        Query<Thread> query = session.createQuery("FROM Thread", Thread.class);
        threads = (List<Thread>) query.list();

        session.getTransaction().commit();
        session.close();
        return threads;
    }

    public List<Thread> getBySectionSorted(Section searched_section) {
        List<Thread> threads;
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        session.beginTransaction();

        Query<Thread> query = session.createQuery("SELECT t FROM Thread t WHERE t.parent_section = :searched_section " +
                                                  "ORDER BY getThreadUpdateTime(t.thread_id)", Thread.class);
        query.setParameter("searched_section", searched_section);
        threads = (List<Thread>) query.list();

        session.getTransaction().commit();
        session.close();
        return threads;
    }
}