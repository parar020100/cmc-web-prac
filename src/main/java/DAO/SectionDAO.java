package DAO;

import entities.User;
import entities.Section;
import entities.Thread;
import entities.Message;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import utils.HibernateSessionFactory;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


public class SectionDAO
{
    public Section findById(int id)
    {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Section section = session.get(Section.class, id);
        session.close();
        return section;
    }

    public Section findByName(String name)
    {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();

        Query<Section> query = session.createQuery("FROM Section WHERE name = :name", Section.class);
        query.setParameter("name", name);
        if (query.getResultList().size() == 0) {
            session.close();
            return null;
        }
        Section section = (Section) query.getSingleResult();
        session.close();

        return section;
    }

    public void addSection(Section section)
    {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction T = session.beginTransaction();
        session.save(section);
        T.commit();
        session.close();
    }

    public void updateSection(Section section)
    {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction T = session.beginTransaction();
        session.update(section);
        T.commit();
        session.close();
    }

    public void deleteSection(Section section)
    {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction T = session.beginTransaction();
        session.delete(section);
        T.commit();
        session.close();
    }

    public List<Section> getAll() {
        List<Section> sections;
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        session.beginTransaction();

        Query<Section> query = session.createQuery("FROM Section", Section.class);
        sections = (List<Section>) query.list();

        session.getTransaction().commit();
        session.close();
        return sections;
    }

    public List<Section> getAllSorted() {
        List<Section> sections;
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        session.beginTransaction();

        Query<Section> query = session.createQuery("FROM Section ORDER BY last_updated", Section.class);
        sections = (List<Section>) query.list();

        session.getTransaction().commit();
        session.close();
        return sections;
    }

}




