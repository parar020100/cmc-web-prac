package DAO;

import entities.User;
import entities.Section;
import entities.Thread;
import entities.Message;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import utils.HibernateSessionFactory;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


public class MessageDAO
{
    public Message findById(int id)
    {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Message message = session.get(Message.class, id);
        session.close();
        return message;
    }

    public void addMessage(Message message)
    {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction T = session.beginTransaction();
        session.save(message);
        T.commit();
        session.close();
    }

    public void updateMessage(Message message)
    {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction T = session.beginTransaction();
        session.update(message);
        T.commit();
        session.close();
    }

    public void deleteMessage(Message message)
    {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction T = session.beginTransaction();
        session.delete(message);
        T.commit();
        session.close();
    }

    public List<Message> getAll() {
        List<Message> messages;
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        session.beginTransaction();

        Query<Message> query = session.createQuery("FROM Message", Message.class);
        messages = (List<Message>) query.list();

        session.getTransaction().commit();
        session.close();
        return messages;
    }

    public List<Message> getAllFromUser(User searched_author) {
        List<Message> messages;
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        session.beginTransaction();

        Query<Message> query = session.createQuery("FROM Message WHERE author = :author ORDER BY sent_time", Message.class);
        query.setParameter("author", searched_author);
        messages = (List<Message>) query.list();

        session.getTransaction().commit();
        session.close();
        return messages;
    }

    public List<Message> getAllFromThread(Thread thread) {
        List<Message> messages;
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        session.beginTransaction();

        Query<Message> query = session.createQuery("FROM Message WHERE parent_thread = :thread ORDER BY sent_time", Message.class);
        query.setParameter("thread", thread);
        messages = (List<Message>) query.list();

        session.getTransaction().commit();
        session.close();
        return messages;
    }

    public void deleteAllFromThread(Thread thread) {
        List<Message> messages;
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        session.beginTransaction();

        Query query = session.createQuery("DELETE FROM Message WHERE parent_thread = :thread");
        query.setParameter("thread", thread);

        session.getTransaction().commit();
        session.close();
        return;
    }
}