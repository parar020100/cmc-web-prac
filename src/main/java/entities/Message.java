package entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table (name = "messages")
public class Message
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int message_id;

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = Thread.class)
    @JoinColumn(name = "parent_thread_id", referencedColumnName = "thread_id")
    private Thread parent_thread;

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = User.class)
    @JoinColumn(name = "author_id", referencedColumnName = "user_id")
    private User author;

    @OneToOne(targetEntity = Thread.class, mappedBy = "initial_message", cascade = CascadeType.ALL, orphanRemoval = false)
    private Thread started_thread;


    private String title;
    private String text;
    private java.sql.Timestamp sent_time;
    //private String attachments;
    private int likes;




    public int getMessage_id() {
        return message_id;
    }
    public void setMessage_id(int message_id) {
        this.message_id = message_id;
    }
    public Thread getParent_thread() {
        return parent_thread;
    }
    public void setParent_thread(Thread parent_thread) {
        this.parent_thread = parent_thread;
    }
    public User getAuthor() {
        return author;
    }
    public void setAuthor(User author) {
        this.author = author;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getText() {
        return text;
    }
    public void setText(String text) {
        this.text = text;
    }
    public Timestamp getSent_time() {
        return sent_time;
    }
    public void setSent_time(Timestamp sent_time) {
        this.sent_time = sent_time;
    }

    public int getLikes() {
        return likes;
    }
    public void setLikes(int likes) {
        this.likes = likes;
    }
    public Thread getStarted_thread() {
        return started_thread;
    }
    public void setStarted_thread(Thread started_thread) {
        this.started_thread = started_thread;
    }

    public Message() {}

    public Message(Thread parent_thread, User author, Thread started_thread, String title, String text, Timestamp sent_time) {
        this.parent_thread = parent_thread;
        this.author = author;
        this.started_thread = started_thread;
        this.title = title;
        this.text = text;
        this.sent_time = sent_time;
        this.likes = 0;


    }
}