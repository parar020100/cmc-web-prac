package entities;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table (name = "threads")
public class Thread
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int thread_id;

    @OneToMany(mappedBy = "parent_thread", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Message> thread_messages;

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = User.class)
    @JoinColumn(name = "thread_creator_id", referencedColumnName = "user_id")
    private User thread_creator;

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = Section.class)
    @JoinColumn(name = "parent_section_id", referencedColumnName = "section_id")
    private Section parent_section;

    @OneToOne(fetch = FetchType.EAGER, targetEntity = Message.class)
    @JoinColumn(name = "initial_message", referencedColumnName = "message_id")
    private Message initial_message;

    private String name;

    public int getThread_id() {
        return thread_id;
    }
    public void setThread_id(int thread_id) {
        this.thread_id = thread_id;
    }
    public Set<Message> getThread_messages() {
        return thread_messages;
    }
    public void setThread_messages(Set<Message> thread_messages) {
        this.thread_messages = thread_messages;
    }
    public User getThread_creator() {
        return thread_creator;
    }
    public void setThread_creator(User thread_creator) {
        this.thread_creator = thread_creator;
    }
    public Section getParent_section() {
        return parent_section;
    }
    public void setParent_section(Section parent_section) {
        this.parent_section = parent_section;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Message getInitial_message() {
        return initial_message;
    }
    public void setInitial_message(Message initial_message) {
        this.initial_message = initial_message;
    }

    public Thread() {}

    public Thread(User thread_creator, Section parent_section, String name) {
        this.thread_creator = thread_creator;
        this.parent_section = parent_section;
        this.name = name;
    }
}