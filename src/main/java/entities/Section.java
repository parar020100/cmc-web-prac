package entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table (name = "sections")
public class Section
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int section_id;

    @OneToMany(mappedBy = "parent_section", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Thread> section_threads;


    @ManyToOne(fetch = FetchType.EAGER, targetEntity = User.class)
    @JoinColumn(name = "section_creator_id", referencedColumnName = "user_id")
    private User section_creator;


    private String name;
    private String description;
    private java.sql.Timestamp last_updated;

    public int getSection_id() {
        return section_id;
    }
    public void setSection_id(int section_id) {
        this.section_id = section_id;
    }
    public Set<Thread> getSection_threads() {
        return section_threads;
    }
    public void setSection_threads(Set<Thread> section_threads) {
        this.section_threads = section_threads;
    }
    public User getSection_creator() {
        return section_creator;
    }
    public void setSection_creator(User section_creator) {
        this.section_creator = section_creator;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public Timestamp getLast_updated() {
        return last_updated;
    }
    public void setLast_updated(Timestamp last_updated) {
        this.last_updated = last_updated;
    }

    public Section() {}

    public Section(User section_creator, String name, String description, Timestamp last_updated) {
        this.section_creator = section_creator;
        this.name = name;
        this.description = description;
        this.last_updated = last_updated;
    }
}
