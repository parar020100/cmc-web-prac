package entities;

import javax.persistence.*;
import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table (name = "users")
public class User
{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int user_id;

    @OneToMany(mappedBy = "section_creator", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Section> user_created_sections;

    @OneToMany(mappedBy = "author", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Message> user_messages;

    @OneToMany(mappedBy = "thread_creator", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Thread> user_created_threads;


    private String login;
    private String password;
    private String nickname;
    private java.sql.Date registration_date;
    private boolean is_moderator;
    private boolean is_banned;


    public int getUser_id() {
        return user_id;
    }
    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }
    public Set<Section> getUser_created_sections() {
        return user_created_sections;
    }
    public void setUser_created_sections(Set<Section> user_created_sections) {
        this.user_created_sections = user_created_sections;
    }
    public Set<Message> getUser_messages() {
        return user_messages;
    }
    public void setUser_messages(Set<Message> user_messages) {
        this.user_messages = user_messages;
    }
    public Set<Thread> getUser_created_threads() {
        return user_created_threads;
    }
    public void setUser_created_threads(Set<Thread> user_created_threads) {
        this.user_created_threads = user_created_threads;
    }
    public String getLogin() {
        return login;
    }
    public void setLogin(String login) {
        this.login = login;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getNickname() {
        return nickname;
    }
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
    public Date getRegistration_date() {
        return registration_date;
    }
    public void setRegistration_date(Date registration_date) {
        this.registration_date = registration_date;
    }
    public boolean isIs_moderator() {
        return is_moderator;
    }
    public void setIs_moderator(boolean is_moderator) {
        this.is_moderator = is_moderator;
    }
    public boolean isIs_banned() {
        return is_banned;
    }
    public void setIs_banned(boolean is_banned) {
        this.is_banned = is_banned;
    }

    public User() {}

    public User(String login, String password, String nickname, Date registration_date) {
        this.login = login;
        this.password = password;
        this.nickname = nickname;
        this.registration_date = registration_date;
        this.is_moderator = false;
        this.is_banned = false;
    }
}