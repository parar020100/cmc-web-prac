<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<table class="layout">
    <tr class="layout">
        <td class="layout" style="width: 70%;">
            <h2> <a href="${pageContext.request.contextPath}/main_page">Форум</a> - ${pagename} </h2>
        </td>
        <td class="layout" style="width: 30%; text-align: end; ">
            <c:choose>
                <c:when test="${auth_nickname == '' || auth_nickname == null}">
                   <a href="${pageContext.request.contextPath}/authorization"> Войти </a>
                </c:when>
                <c:otherwise>

                    <a href="${pageContext.request.contextPath}/profile?user_id=${auth_id}">
                        ${auth_nickname}
                    </a>
                    <br>
                    <a href="${pageContext.request.contextPath}/logout">
                        Выйти
                    </a>
                </c:otherwise>
            </c:choose>
        </td>
    </tr>
</table>