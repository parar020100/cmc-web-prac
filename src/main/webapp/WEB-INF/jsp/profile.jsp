<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="include.jsp" %>
<html>
<head>
    <title>Форум</title>
</head>
<body>
    <%@ include file="head.jsp" %>

    Дата регистрации: ${user.registration_date}
    <br>
    Права модератора:
    <c:if test="${user.is_moderator == true}"> есть </c:if>
    <c:if test="${user.is_moderator == false}"> нет </c:if>
    <br>
    Заблокирован:
    <c:if test="${user.is_banned == true}"> да </c:if>
    <c:if test="${user.is_banned == false}"> нет </c:if>
    <c:if test="${auth_moderator == true}">
        <c:if test="${user.is_banned == false}">
            <a href="${pageContext.request.contextPath}/ban_user?user_id=${user.user_id}">
                [заблокировать]
            </a>
        </c:if>
        <c:if test="${user.is_banned == true}">
            <a href="${pageContext.request.contextPath}/pardon_user?user_id=${user.user_id}">
                [разблокировать]
            </a>
        </c:if>
    </c:if>


    <br>
    Недавние сообщения:
    <br>
    <br>


    <table class="list" style="width: 85%">
        <tr>
            <th class="list">
                Время:
            </th>
            <th class="list">
                Раздел:
            </th>
            <th class="list">
                Тема:
            </th>
            <th class="list">
                Текст:
            </th>
        </tr>
        <c:forEach var="message" items="${messages}">
            <tr>
                <td class="list">
                    ${message.sent_time}
                </td>
                <td class="list">
                    <a href="${pageContext.request.contextPath}/section?section_id=${message.parent_thread.parent_section.section_id}">
                        ${message.parent_thread.parent_section.name}
                    </a>
                </td>
                <td class="list">
                    <a href="${pageContext.request.contextPath}/thread?thread_id=${message.parent_thread.thread_id}">
                        ${message.parent_thread.name}
                    </a>
                </td>
                <td class="list">
                        ${message.text}
                </td>
            </tr>
        </c:forEach>
    </table>





</body>
</html>
