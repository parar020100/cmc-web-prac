<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="include.jsp" %>
<html>
<head>
    <title>Новое обсуждение</title>
</head>
<body>

<form id="new_thread_form" action="/new_thread" method="post" class="auth" style="width: 60%">

    <br>
    <label for="new_thread_section" >
        Раздел:
    </label>
    <br>

    <select id="new_thread_section" name="new_thread_section" style="width: 95%" required>
        <c:forEach var="section" items="${sections}">
            <c:if test="${section.section_id == section_id}">
                <option value="${section.section_id}" label="${section.name}" selected>
            </c:if>
            <c:if test="${section.section_id != section_id}">
                <option value="${section.section_id}" label="${section.name}">
            </c:if>
        </c:forEach>
    </select>




    <br>
    <br>
    <label for="new_thread_name" >
        Название нового обсуждения:
    </label>
    <br>
    <input type="text" id="new_thread_name" name="new_thread_name" style="width: 95%" required>
    <br>
    <br>
    <label for="new_thread_firstmsg" >
        Сообщение:
    </label>
    <br>
    <textarea type="text" id="new_thread_firstmsg" name="new_thread_firstmsg" style="width: 95%" required></textarea>
    <br>
    <br>
    <button type="submit">Создать</button>
    <br>
    <br>
    <a href="${pageContext.request.contextPath}/section?section_id=${section_id}" style="font-size: 70%">
        Назад
    </a>
    <br>
    <br>
</form>

</body>
</html>
