<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="include.jsp" %>
<html>
<head>
    <title>Обсуждение</title>
</head>
<body>

<%@ include file="head.jsp" %>



<br>
<br>


<table class="list" style="width: 95%; margin: auto">
    <tr>
        <th class="list" style="width: 10%; border-right-style: dotted;">
            Время:
        </th>
        <th class="list" style="width: 10%; border-right-style: dotted;">
            Пользователь:
        </th>
        <c:if test="${auth_moderator == true}">
            <th class="list" style="width: 75%;">
                Сообщение:
            </th>
            <th class="list" style="width: 5%;">
            </th>
        </c:if>
        <c:if test="${auth_moderator != true}">
            <th class="list" style="width: 80%;">
                Сообщение:
            </th>
        </c:if>
    </tr>
    <c:forEach var="message" items="${messages}">
        <tr>
            <td class="list" style="width: 10%; border-right-style: dotted;">
                ${message.sent_time}
            </td>
            <td class="list" style="width: 10%; border-right-style: dotted;">
                <a href="${pageContext.request.contextPath}/profile?user_id=${message.author.user_id}">
                    ${message.author.nickname}
                </a>
                <c:if test="${message.author.is_banned == true}">
                    <br>
                    <a style="font-size: 50%; color: red;">
                        [ЗАБЛОКИРОВАН]
                    </a>
                </c:if>
            </td>
            <c:if test="${auth_moderator == true}">
                <td class="list" style="width: 75%;">
                    <p style="font-weight: bold">
                            ${message.title}
                    </p>
                    ${message.text}
                </td>
                <td class="list" style="width: 5%;">
                    <c:if test="${thread.initial_message.message_id != message.message_id}">
                        <a href="${pageContext.request.contextPath}/delete_message?message_id=${message.message_id}">
                            [X]
                        </a>
                    </c:if>
                </td>
            </c:if>
            <c:if test="${auth_moderator != true}">
                <td class="list" style="width: 80%;">
                    <p style="font-weight: bold">
                        ${message.title}
                    </p>
                    ${message.text}
                </td>
            </c:if>
        </tr>
    </c:forEach>
</table>

<c:if test="${auth_nickname != null}">
    <form id="new_message_form" action="/send_message?thread_id=${thread.thread_id}" method="post" class="auth" style="width: 95%; margin: auto;">

        <br>
        <label for="new_message_title" >
            Заголовок сообщения:
        </label>
        <br>
        <input type="text" id="new_message_title" name="new_message_title" style="width: 95%">
        <br>
        <br>
        <label for="new_message_text" >
            Текст сообщения:
        </label>
        <br>
        <textarea type="text" id="new_message_text" name="new_message_text" style="width: 95%; height: 40%" required></textarea>
        <br>
        <button type="submit">Отправить</button>
        <br>
        <br>
    </form>
</c:if>



</body>
</html>
