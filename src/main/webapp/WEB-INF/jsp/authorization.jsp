<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="include.jsp" %>
<html>
<head>
    <title>Вход</title>
</head>
<body>

    <form id="authorize_form" action="/authorization" method="post" class="auth">

        <br>
        <label for="login" >Логин</label> <br>
        <input type="text" id="login" name="login" required>

        <br>
        <br>

        <label for="password" >Пароль</label><br>
        <input type="password" id="password" name="password" required>

        <br><br>

        <c:if test="${auth_failed == true}">
            <label style="color: red; font-size: 70%;">
                Неправильный логин или пароль
            </label>
            <br><br>
        </c:if>
        <c:if test="${auth_banned == true}">
            <label style="color: red; font-size: 70%;">
                Данный аккаунт был заблокирован
            </label>
            <br><br>
        </c:if>

        <c:if test="${auth_banned != true}">
            <button type="submit">
                Войти
            </button>
            <br>
            <br>
            <a href="${pageContext.request.contextPath}/new_user" style="font-size: 70%">
                Регистрация
            </a>
            <br>
        </c:if>
        <a href="${pageContext.request.contextPath}/main_page" style="font-size: 70%">
            Вернуться на главную
        </a>
        <br>
        <br>
    </form>

</body>
</html>
