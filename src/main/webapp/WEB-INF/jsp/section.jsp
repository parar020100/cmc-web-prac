<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="include.jsp" %>
<html>
<head>
    <title>Раздел</title>
</head>
<body>

    <%@ include file="head.jsp" %>

    Описание раздела: ${description}
    <br>
    <br>

    <table class="layout">
        <tr class="layout">
            <td class="layout" style="width: 30%;">
                <table class="list">
                    <tr>
                        <th class="list">
                            Пользователи:
                        </th>
                    </tr>
                    <c:forEach var="user" items="${users}">
                        <tr>
                            <td class="list">
                                <a href="${pageContext.request.contextPath}/profile?user_id=${user.user_id}">
                                        ${user.nickname}
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </td>
            <td class="layout" style="width: 70%;">

                <c:if test="${auth_nickname != null}">
                    <table class="list">
                        <tr class="list">
                            <td class="list">
                                <a href="${pageContext.request.contextPath}/new_thread?section_id=${section_id}">
                                    Создать новое обсуждение
                                </a>
                            </td>
                            <c:if test="${auth_moderator == true}">
                                <td class="list" style="width: 10%;">
                                </td>
                            </c:if>
                        </tr>
                    </table>
                    <br>
                </c:if>

                <table class="list">
                    <tr class="list">
                        <th class="list">
                            Обсуждения:
                        </th>
                        <c:if test="${auth_moderator == true}">
                            <th class="list" style="width: 10%">
                            </th>
                        </c:if>
                    </tr>
                    <c:forEach var="thread" items="${threads}">
                        <tr class="list">
                            <td class="list">
                                <a href="${pageContext.request.contextPath}/thread?thread_id=${thread.thread_id}">
                                        ${thread.name}
                                </a>
                            </td>
                            <c:if test="${auth_moderator == true}">
                                <td class="list">
                                    <a href="${pageContext.request.contextPath}/delete_thread?thread_id=${thread.thread_id}">
                                        [X]
                                    </a>
                                </td>
                            </c:if>
                        </tr>
                    </c:forEach>
                </table>
            </td>
        </tr>
    </table>


</body>
</html>
