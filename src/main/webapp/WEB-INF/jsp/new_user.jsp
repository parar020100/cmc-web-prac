<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="include.jsp" %>
<html>
<head>
    <title>Новый пользователь</title>
</head>
<body>

<form id="new_user_form" action="/new_user" method="post" class="auth">

    <br>
    <label for="new_user_login" >
        Логин
    </label>
    <br>
    <input type="text" id="new_user_login" name="new_user_login" required>
    <br>
    <br>
    <label for="new_user_nickname" >
        Никнейм
    </label>
    <br>
    <input type="text" id="new_user_nickname" name="new_user_nickname" required>
    <br>
    <br>
    <label for="new_user_password" >
        Пароль
    </label><br>
    <input type="password" id="new_user_password" name="new_user_password" required>
    <br>
    <br>
    <label for="new_user_repeat_password" >
        Повторите пароль
    </label>
    <br>
    <input type="password" id="new_user_repeat_password" name="new_user_repeat_password" required>
    <br>
    <br>
    <c:if test="${creation_failed == true}">
        <label style="color: red; font-size: 70%;">
            Ошибка! Пожалуйста, проверьте данные
        </label>
        <br>
        <br>
    </c:if>
    <button type="submit">Создать</button>
    <br>
    <br>
    <a href="${pageContext.request.contextPath}/authorization" style="font-size: 70%">
        Назад
    </a>
    <br>
    <br>
</form>

</body>
</html>
