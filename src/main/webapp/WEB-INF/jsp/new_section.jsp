<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="include.jsp" %>
<html>
<head>
    <title>Новый раздел</title>
</head>
<body>

<form id="new_section_form" action="/new_section" method="post" class="auth" style="width: 60%">

    <br>
    <label for="new_section_name" >
        Название нового раздела:
    </label>
    <br>
    <input type="text" id="new_section_name" name="new_section_name" style="width: 95%" required>
    <br>
    <br>
    <label for="new_section_desc" >
        Описание:
    </label>
    <br>
    <textarea type="text" id="new_section_desc" name="new_section_desc" style="width: 95%" required>
    </textarea>
    <br>
    <br>
    <button type="submit">Создать</button>
    <br>
    <br>
    <a href="${pageContext.request.contextPath}/main_page" style="font-size: 70%">
        Назад
    </a>
    <br>
    <br>
</form>

</body>
</html>
