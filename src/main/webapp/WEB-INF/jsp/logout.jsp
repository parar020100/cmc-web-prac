<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="include.jsp" %>
<html>
<head>
    <title>Выход</title>
</head>
<body>

    <form id="logout_form" action="/logout" method="post" class="auth">
        <br>
        <br>
        <label>
            Вы точно хотите выйти?
        </label>
        <br>
        <br>
        <button type="submit">
            Выйти
        </button>
        <br>
        <br>
        <a href="${pageContext.request.contextPath}/main_page" style="font-size: 70%">
            Вернуться на главную
        </a>
        <br>
        <br>
    </form>

</body>
</html>

