CREATE DATABASE forum;

\connect forum;

-- Creating tables
BEGIN;

	CREATE TABLE IF NOT EXISTS users(
		user_id serial PRIMARY KEY,
		login varchar(128) UNIQUE NOT NULL,
		password varchar(32) NOT NULL,
		nickname varchar(32) UNIQUE NOT NULL,
		registration_date date NOT NULL,
		is_moderator boolean DEFAULT false,
		is_banned boolean DEFAULT false
	);

	CREATE TABLE IF NOT EXISTS sections(
		section_id serial PRIMARY KEY,
		section_creator_id int REFERENCES users(user_id),
		name varchar(256) UNIQUE NOT NULL,
		description text,
		last_updated timestamp
	);

	CREATE TABLE IF NOT EXISTS messages(
		message_id serial PRIMARY KEY,
		parent_thread_id int,
		author_id int NOT NULL REFERENCES users(user_id),
		title varchar(256) NULL,
		text text,
		sent_time timestamp,
		--attachments json DEFAULT '[]',
		likes int CHECK (likes >= 0) DEFAULT 0
	);

	CREATE TABLE IF NOT EXISTS threads(
		thread_id serial PRIMARY KEY,
		thread_creator_id int REFERENCES users(user_id),
		parent_section_id int NOT NULL REFERENCES sections(section_id),
		name varchar(256),
		initial_message int UNIQUE REFERENCES messages(message_id)
	);

	ALTER TABLE messages
		ADD FOREIGN KEY (parent_thread_id)
		REFERENCES threads(thread_id);


    CREATE OR REPLACE FUNCTION getRecentUserActivity(user_id int, days int)
        RETURNS int AS $$
            SELECT COUNT(*) FROM messages
                WHERE author_id = user_id AND sent_time::date > current_date - days
        $$ LANGUAGE SQL;

    CREATE OR REPLACE FUNCTION getUserSectionActivity(user_id int, searched_section_id int)
        RETURNS int AS $$
            SELECT COUNT(*) FROM messages JOIN threads ON (messages.parent_thread_id = threads.thread_id)
                WHERE author_id = user_id AND searched_section_id = parent_section_id
        $$ LANGUAGE SQL;

    CREATE OR REPLACE FUNCTION getThreadUpdateTime(thread int)
        RETURNS timestamp AS $$
    SELECT MAX(sent_time) FROM messages JOIN threads ON (messages.parent_thread_id = threads.thread_id)
    WHERE threads.thread_id = thread
    $$ LANGUAGE SQL;

    CREATE OR REPLACE FUNCTION UpdateSectionTime()
        RETURNS trigger
    AS $$
    BEGIN
        UPDATE sections
        SET last_updated = (
            SELECT MAX(getThreadUpdateTime(thread_id))
            FROM Threads
            WHERE parent_section_id = sections.section_id
        );
        RETURN NEW;
    END;
    $$ LANGUAGE plpgsql;

    CREATE TRIGGER update_section_timestamp
        AFTER UPDATE ON messages
    EXECUTE FUNCTION UpdateSectionTime();

COMMIT;

