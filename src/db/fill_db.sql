\connect forum;
SET datestyle TO German;

BEGIN;
	INSERT INTO Users (login, password, nickname, registration_date)
		VALUES
			('admin@cs.msu.ru', 'Practicum2021', 'BOSS', '02.01.2000'),
			('helper@cs.msu.ru', 'Practicum2021-help', 'Moderator', '03.01.2000'),
			('banned@cs.msu.ru', 'does_not_matter', 'Spammer03', '14.07.2020'),
			('another_user@rambler.ru', '5891fk1w', 'Adrian', '29.07.2020'),
			('elena.elenovna.elennaya@yandex.ru', 'elena_cool', 'Helena', '15.08.2021'),
			('a.a@a.ru', 'qwerty', 'AAA', '15.08.2021');

	UPDATE Users
		SET is_moderator = true WHERE user_id = 1 OR user_id = 2;

	UPDATE Users
		SET is_banned = true WHERE user_id = 3;

	INSERT INTO Sections (section_creator_id, name, description, last_updated)
		VALUES
			(1, 'Miscellaneous', 'Any general topics', '02.01.2000 01:00:00'),
			(1, 'PostgreSQL', 'Discussions related to PostgreSQL', '27.01.2020 14:26:04'),
			(2, 'Chess', 'Chess games and discussions', '27.06.2010 10:03:56');

	INSERT INTO Threads (thread_creator_id, parent_section_id, name)
		VALUES
			(1, 1, 'Choice of sections'),
			(3, 1, 'SPAMSPAMSPAM'),
			(3, 2, 'SPAMSPAMSPAMSPAM'),
			(3, 1, 'IREALLYLIKETOSPAM'),
			(5, 1, 'Looking for a husband');

	INSERT INTO Messages (parent_thread_id, author_id, title, text, sent_time)
		VALUES
			(2, 3, 'LALALALA-SPAM', 'HAHAHAHAHAHAHAHAHAHAHAHA', '14.07.2020 10:00:01'),
			(2, 3, 'MORE-SPAM', 'AHAHA', '14.07.2020 10:00:02'),
			(2, 3, 'ILOVETOSPAM', 'You will never get out of my spam trap', '14.07.2020 10:00:03'),
			(2, 3, 'SPAM', 'HAHAHAHAHAHAHAHAHAHAHAHA', '14.07.2020 10:00:04'),
			(2, 3, NULL, 'AHAHAHAHAHAHAHAHAHAHAHAHA', '14.07.2020 10:00:05'),
			(2, 3, NULL, 'Try to ban me', '14.07.2020 10:00:17'),
			(2, 3, 'VERY CLEVER COMMENT', 'Moderators suck', '14.07.2020 10:00:18'),
			(3, 3, 'SOME MORE SPAM', 'Moderators are losers', '14.07.2020 10:00:19'),
			(4, 3, 'EVEN MORE SPAM LMAO', 'Moderators are losers', '14.07.2020 10:00:20'),
			(4, 3, 'LALALALA-SOME MORE SPAM', 'Moderators are losers', '14.07.2020 10:00:21'),

			(1, 1, 'Looking for your suggestions', 'I suggest we discuss, which sections this forum needs', '29.07.2020 18:49:06'),
			(1, 4, 'PostgreSQL perhaps?', 'I really like the community of PostgreSQL, I think this forum needs a section for its members', '30.07.2020 13:21:34'),
			(1, 4, NULL, 'Also, what about chess lovers?', '30.07.2020 13:22:11'),
			(1, 2, NULL, 'Agreed, chess is a great game', '30.07.2020 17:28:24'),
			(1, 3, 'Why not?', 'What if I just start spamming?', '30.07.2020 18:03:57'),
			(1, 6, NULL, 'Please, do not spam', '04.09.2020 07:33:21'),

			(5, 5, 'HI', 'Hi, I am Helen, aged 84, and I want to find a husband here! I live with 10 cats, and am not shy to admit it, but they are all sweet!', '15.08.2021 19:01:04'),
			(5, 5, 'Hi again!', 'Why is nobody replying???', '15.08.2021 19:02:00');

	--UPDATE Messages
	--	SET attachments = '
	--		[
	--			{
	--				"type": "img",
	--				"src": "https://yandex-images.clstorage.net/bmo47Y286/09441frkhf/aut1CCmye0hh3yUiLfX1N8zZoqzATHwUxn8vzBAwJc48yQfoH80GKi6XZuw9O2e0KF_38ZVYD5SzAPrmrPFcWDrqa1o9vSYBcYksfnLOzw6j9ZiXvWwENOw7d5ZCNmTYLN071QugUnR9uy-1d1ty53sKkf6AIyLn9WuMyJDwLlREiwI0EJMzTHeIGDMy4P8sgPOJuY53GyfGEFW2dRVVkhzBDMhfnG19ZrAatFhdbt1hIq__gDI_69dimuKLzwRSQoM2ESiHB0Jeqw07DcTnCLHTgbaNYg0f8i5Zi0cBcrcT0Tb8TIBKV1SFAp9DfWfjfgWs97sGFcjsXrT2mMoVXm-zCAUWtwROW5AQAAbZ6jeg7a22hmAXGcEKWqhqI0SPPOUSxkiJXGh8kA2MTEVEw34Ot_qMMQrn43-7wrH3NV91jhQZH6U7bleLEAgR-eomsNOMk6NvNSj2N3Kdax1rlQ_AKfhDtHBeZr4enkxKUuNKJpL1kj068sFdm8yPzwJjRLMUKjK4JFVfpA4SCNXUC5HPho2bQA4e7C5vmWs5Xp4R-hf7eKxSU0a8NYlcYEDOehuK1o4kL8P4e67uleMNfFKfOzg1ijdqfIg2CjXI1CCWwrK1lmQUJ8EWcbxTCU2PJPkA6Uqxcn99hw6ZbGVh1lYSsNWGAA7Fz2eY0pTEKn1xjg4MP7gXSmGHJQAL5fUcg8WlrKJ2DT7pMXSsTjlxny3CJtJ7kXN3dLoHnmZAV_1_IIzlvhw05OFwmciL4j9dZpgJCR-FG3BCuxgmNPfcDoP2tbmsZDoNwwtSnHckTbQHziPyRJ1bQXq1HaV1Y1DTRh2G-qIuJf_Dd5rqtOkDRGmIOR0qmCVXfLQOABbW5iyz25WgjHwhEuw1WaJ-PX-EGNIAxlirW2xsqCOTX2JL1Gges9-_HiTj032yzaboI0JFkC0XDIEfblqAMDA14sQZssCdsZZcBCTEOnugZD4"
	--			}
	--		]'
	--	WHERE message_id = 17;

COMMIT;

